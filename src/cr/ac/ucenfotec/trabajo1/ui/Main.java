package cr.ac.ucenfotec.trabajo1.ui;

import cr.ac.ucenfotec.trabajo1.bl.Cliente;
import cr.ac.ucenfotec.trabajo1.bl.Producto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class Main {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static Cliente[] clientes;
    static Producto[] productos;

    public static void main(String[] args) throws IOException {
        out.println("Trabajo en clase #1");
        out.println("");

        out.print("Cuantos clientes quieres guardar?: ");
        int cantidadClientes = Integer.parseInt(in.readLine());
        clientes = new Cliente[cantidadClientes];

        out.print("Cuantos productos quieres guardar? :");
        int cantidadProductos = Integer.parseInt(in.readLine());
        productos = new Producto[cantidadProductos];

        int opcion = 0;
        do {
            opcion = pintarMenu();
            switch (opcion) {
                case 1:
                    agregarCliente();
                    break;
                case 2:
                    mostrarClientes();
                    break;
                case 3:
                    agregarProductos();
                break;
                case 4:
                    mostrarProductos();
                break;
            }
        } while(opcion != 0);
    }

    public static int pintarMenu() throws IOException {
        out.println("-------MENU--------");
        out.println("1. Agregar cliente");
        out.println("2. Mostrar clientes");
        out.println("3. Agregar producto");
        out.println("4. Mostrar productos");
        out.println("0. Salir");
        out.print("> ");
        int opcion = Integer.parseInt(in.readLine());
        return opcion;
    }

    public static void mostrarClientes() {
        out.println("");
        out.print("Clientes: ");
        for (int i = 0; i < clientes.length; i++) {
            out.print(clientes[i]);
            out.print(", ");
        }
        out.println("");
        out.println("");
    }

    public static void agregarCliente() throws IOException {
        for (int i = 0; i < clientes.length; i++) {
            if(clientes[i] == null) {

                out.print("Nombre: ");
                String nombre = in.readLine();
                out.print("Cedula: ");
                String cedula = in.readLine();

                Cliente cliente = new Cliente(nombre, cedula);

                clientes[i] = cliente;
                break;
            }
        }
    }

    public static void agregarProductos() throws IOException {
        for (int i = 0; i < productos.length; i++) {
            if(productos[i] == null) {

                out.print("Nombre: ");
                String nombre = in.readLine();
                out.print("Marca: ");
                String marca = in.readLine();

                Producto producto = new Producto(nombre, marca);

                productos[i] = producto;
                break;
            }
        }
    }

    public static void mostrarProductos() {
        out.println("");
        out.print("Productos: ");
        for (int i = 0; i < productos.length; i++) {
            out.print(productos[i]);
            out.print(", ");
        }
        out.println("");
        out.println("");
    }
}
