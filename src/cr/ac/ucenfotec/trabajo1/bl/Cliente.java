package cr.ac.ucenfotec.trabajo1.bl;

import java.util.ArrayList;

public class Cliente {
    private String nombre;
    private String cedula;
    private ArrayList<String> productos = new ArrayList<>();

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        if(nombre.length() >= 3) {
            this.nombre = nombre;
        }
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public ArrayList<String> getProductos() {
        return productos;
    }

    public void setProductos(ArrayList<String> productos) {
        this.productos = productos;
    }

    public Cliente(String nombre, String cedula) {
        setNombre(nombre);
        setCedula(cedula);
    }

    @Override
    public String toString() {
        return nombre + "/" + cedula;
    }
}
